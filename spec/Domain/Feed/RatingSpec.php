<?php

declare(strict_types=1);

namespace spec\App\Domain\Feed;

use App\Domain\Feed\Rating;
use PhpSpec\ObjectBehavior;

class RatingSpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedThrough('fromFloat', [3.5]);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Rating::class);
    }

    public function it_doesnt_allow_ratings_grater_than_5(): void
    {
        $this->beConstructedThrough('fromFloat', [5.1]);

        $this
            ->shouldThrow(\InvalidArgumentException::class)
            ->duringInstantiation();
    }

    public function it_doesnt_allow_ratings_lower_than_1(): void
    {
        $this->beConstructedThrough('fromFloat', [0.9]);

        $this
            ->shouldThrow(\InvalidArgumentException::class)
            ->duringInstantiation();
    }
}
