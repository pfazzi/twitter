<?php

declare(strict_types=1);

namespace spec\App\Domain\Feed;

use App\Domain\Feed\Item;
use App\Domain\Feed\Rating;
use App\Domain\Tweet\Tweet;
use App\Domain\User\User;
use PhpSpec\ObjectBehavior;
use Ramsey\Uuid\UuidInterface;

class ItemSpec extends ObjectBehavior
{
    public function let(
        UuidInterface $uuid,
        User $recipient,
        Tweet $tweet,
        Rating $rating
    ): void {
        $this->beConstructedThrough(
            'create',
            [$uuid, $recipient, $tweet, $rating]
        );
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Item::class);
    }
}
