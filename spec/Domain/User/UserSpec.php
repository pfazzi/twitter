<?php

declare(strict_types=1);

namespace spec\App\Domain\User;

use App\Domain\User\Email;
use App\Domain\User\User;
use App\Domain\User\Username;
use PhpSpec\ObjectBehavior;
use Ramsey\Uuid\UuidInterface;

class UserSpec extends ObjectBehavior
{
    public function let(
        UuidInterface $uuid,
        Username $username,
        Email $email
    ): void {
        $this->beConstructedWith(
            $uuid,
            $username,
            $email
        );
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(User::class);
    }

    public function it_returns_data_it_has_been_constructed_with(
        UuidInterface $uuid,
        Username $username,
        Email $email
    ): void {
        $this->getUuid()->shouldBe($uuid);
        $this->getUsername()->shouldBe($username);
        $this->getEmail()->shouldBe($email);
    }
}
