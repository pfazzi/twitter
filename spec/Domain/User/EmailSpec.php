<?php

declare(strict_types=1);

namespace spec\App\Domain\User;

use App\Domain\User\Email;
use PhpSpec\ObjectBehavior;

class EmailSpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith('pfazzi@example.com');
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Email::class);
    }

    public function it_returns_its_value_as_a_string(): void
    {
        $this->toString()->shouldBe('pfazzi@example.com');
    }

    public function it_can_be_instantiated_by_a_named_constructor(): void
    {
        $this->beConstructedThrough('fromString', ['patrick@example.com']);

        $this->shouldHaveType(Email::class);
    }

    public function it_throws_an_error_if_supplied_value_is_not_a_valid_email_address(): void
    {
        $this->beConstructedWith('An invalid email address');

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }
}
