<?php

declare(strict_types=1);

namespace spec\App\Domain\User;

use App\Domain\User\Username;
use PhpSpec\ObjectBehavior;

class UsernameSpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith('pfazzi');
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Username::class);
    }

    public function it_returns_its_value_as_a_string(): void
    {
        $this->toString()->shouldBe('pfazzi');
    }

    public function it_can_be_instantiated_by_a_named_constructor(): void
    {
        $this->beConstructedThrough('fromString', ['patrick']);

        $this->shouldHaveType(Username::class);
    }
}
