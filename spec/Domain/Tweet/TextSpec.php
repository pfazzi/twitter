<?php

declare(strict_types=1);

namespace spec\App\Domain\Tweet;

use App\Domain\Tweet\Text;
use PhpSpec\ObjectBehavior;

class TextSpec extends ObjectBehavior
{
    public function let(): void
    {
        $this->beConstructedWith('This is a Tweet!');
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Text::class);
    }

    public function it_returns_its_value_as_a_string(): void
    {
        $this->toString()->shouldBe('This is a Tweet!');
    }

    public function it_can_be_instantiated_by_a_named_constructor(): void
    {
        $this->beConstructedThrough('fromString', ['This is another Tweet!']);

        $this->shouldHaveType(Text::class);
    }

    public function it_throws_an_error_if_text_exceeds_100_characters(): void
    {
        $tooLongTweet = str_pad('', 101);

        $this->beConstructedWith($tooLongTweet);

        $this->shouldThrow(\InvalidArgumentException::class)->duringInstantiation();
    }
}
