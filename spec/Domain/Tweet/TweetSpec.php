<?php

declare(strict_types=1);

namespace spec\App\Domain\Tweet;

use App\Domain\Tweet\Text;
use App\Domain\Tweet\Tweet;
use App\Domain\User\User;
use PhpSpec\ObjectBehavior;
use Ramsey\Uuid\UuidInterface;

class TweetSpec extends ObjectBehavior
{
    public function let(
        UuidInterface $uuid,
        Text $text,
        \DateTimeImmutable $createdAt,
        User $author
    ): void {
        $this->beConstructedThrough('publish', [
            $uuid,
            $text,
            $createdAt,
            $author,
        ]);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(Tweet::class);
    }

    public function it_returns_the_time_it_has_been_created(
        \DateTimeImmutable $createdAt
    ): void {
        $this->getCreatedAt()->shouldBeEqualTo($createdAt);
    }

    public function it_returns_its_uuid(
        UuidInterface $uuid
    ): void {
        $this->getUuid()->shouldBeEqualTo($uuid);
    }

    public function it_returns_its_text(
        Text $text
    ): void {
        $this->getText()->shouldBeEqualTo($text);
    }

    public function it_returns_its_author(
        User $author
    ): void {
        $this->getAuthor()->shouldBeEqualTo($author);
    }

    public function it_allows_to_change_the_text(
        Text $newText
    ): void {
        $this->changeText($newText);

        $this->getText()->shouldReturn($newText);
    }
}
