<?php

declare(strict_types=1);

namespace spec\App\Interfaces\Http\Rest\Controller;

use App\Domain\Tweet\Tweet;
use App\Domain\Tweet\TweetRepositoryInterface;
use App\Interfaces\Http\Rest\Controller\FeedController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FeedControllerSpec extends ObjectBehavior
{
    public function let(
        ViewHandlerInterface $viewHandler,
        TweetRepositoryInterface $itemRepository
    ): void {
        $this->beConstructedWith(
            $viewHandler,
            $itemRepository
        );
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(FeedController::class);
    }

    public function it_retrieves_a_page_from_items_repo_and_returns_it_as_a_view(
        Request $request,
        ViewHandlerInterface $viewHandler,
        TweetRepositoryInterface $itemRepository,
        Tweet $tweet
    ): void {
        $request->get('userUuid')->willReturn('351b9778-9f2f-4d0d-a8cc-6487e7418027');
        $request->get('page', 1)->willReturn(2);
        $request->get('limit', 10)->willReturn(25);

        $itemRepository->pagedResultForUser(
            Argument::type(Uuid::class),
            2,
            25,
        )->willReturn([$tweet]);

        $viewHandler
            ->handle(Argument::type(View::class))
            ->willReturn(Response::create())
            ->shouldBeCalled();

        $this->__invoke($request);
    }
}
