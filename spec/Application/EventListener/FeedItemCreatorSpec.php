<?php

declare(strict_types=1);

namespace spec\App\Application\EventListener;

use App\Application\Command\Feed\PublishTweetToFeeds;
use App\Application\EventListener\FeedItemCreator;
use App\Domain\Tweet\TweetPublished;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class FeedItemCreatorSpec extends ObjectBehavior
{
    public function let(MessageBusInterface $messageBus): void
    {
        $this->beConstructedWith($messageBus);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(FeedItemCreator::class);
    }

    public function it_triggers_feed_items_creations(MessageBusInterface $messageBus): void
    {
        $messageBus
            ->dispatch(Argument::type(PublishTweetToFeeds::class))
            ->willReturn(new Envelope(new \stdClass()))
            ->shouldBeCalled();

        $this->__invoke(new TweetPublished(Uuid::uuid4()));
    }
}
