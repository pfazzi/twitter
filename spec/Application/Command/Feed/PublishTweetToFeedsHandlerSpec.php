<?php

declare(strict_types=1);

namespace spec\App\Application\Command\Feed;

use App\Application\Command\Feed\PublishTweetToFeeds;
use App\Application\Command\Feed\PublishTweetToFeedsHandler;
use App\Domain\Feed\Item;
use App\Domain\Feed\ItemRepositoryInterface;
use App\Domain\Feed\Rating;
use App\Domain\Feed\RatingCalculatorInterface;
use App\Domain\Tweet\Tweet;
use App\Domain\Tweet\TweetRepositoryInterface;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use ArrayIterator;
use PhpOption\Option;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\UuidInterface;

class PublishTweetToFeedsHandlerSpec extends ObjectBehavior
{
    public function let(
        RatingCalculatorInterface $ratingCalculator,
        UserRepositoryInterface $userRepository,
        TweetRepositoryInterface $tweetRepository,
        ItemRepositoryInterface $itemRepository
    ): void {
        $this->beConstructedWith(
            $ratingCalculator,
            $userRepository,
            $tweetRepository,
            $itemRepository
        );
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(PublishTweetToFeedsHandler::class);
    }

    public function it_adds_tweet_to_users_feed(
        RatingCalculatorInterface $ratingCalculator,
        UserRepositoryInterface $userRepository,
        TweetRepositoryInterface $tweetRepository,
        ItemRepositoryInterface $itemRepository,
        UuidInterface $tweetUuid,
        Option $tweetOption,
        Tweet $tweet,
        User $pfazzi,
        User $patrick
    ): void {
        $command = new PublishTweetToFeeds($tweetUuid->getWrappedObject());

        $tweetOption->getOrThrow(Argument::any())->willReturn($tweet);
        $tweetRepository->get($tweetUuid)->willReturn($tweetOption);

        $userRepository->all()->willReturn(new ArrayIterator([
            $pfazzi->getWrappedObject(),
            $patrick->getWrappedObject(),
        ]));

        $ratingCalculator
            ->calculateRateFor($tweet, Argument::type(User::class))
            ->shouldBeCalledTimes(2)
            ->will(fn () => Rating::fromFloat(random_int(1, 5)));

        $itemRepository->store(Argument::type(Item::class))->shouldBeCalledTimes(2);

        $this->__invoke($command);
    }
}
