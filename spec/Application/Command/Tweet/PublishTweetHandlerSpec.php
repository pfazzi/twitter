<?php

declare(strict_types=1);

namespace spec\App\Application\Command\Tweet;

use App\Application\Command\Tweet\PublishTweet;
use App\Application\Command\Tweet\PublishTweetHandler;
use App\Domain\Shared\ClockInterface;
use App\Domain\Shared\EventDispatcherInterface;
use App\Domain\Shared\NotFoundException;
use App\Domain\Tweet\Text;
use App\Domain\Tweet\Tweet;
use App\Domain\Tweet\TweetPublished;
use App\Domain\Tweet\TweetRepositoryInterface;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use PhpOption\Option;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Ramsey\Uuid\UuidInterface;

class PublishTweetHandlerSpec extends ObjectBehavior
{
    public function let(
        UserRepositoryInterface $userRepository,
        TweetRepositoryInterface $tweetRepository,
        ClockInterface $clock,
        EventDispatcherInterface $eventDispatcher
    ): void {
        $this->beConstructedWith($userRepository, $tweetRepository, $clock, $eventDispatcher);
    }

    public function it_is_initializable(): void
    {
        $this->shouldHaveType(PublishTweetHandler::class);
    }

    public function it_creates_a_new_tweet_and_persists_it_emitting_an_event(
        PublishTweet $command,
        TweetRepositoryInterface $tweetRepository,
        UserRepositoryInterface $userRepository,
        UuidInterface $authorUuid,
        Option $maybeUser,
        User $author,
        ClockInterface $clock,
        EventDispatcherInterface $eventDispatcher
    ): void {
        $command->getAuthorUuid()->willReturn($authorUuid);
        $command->getText()->willReturn(Text::fromString('Lorem ipsum'));

        $userRepository->get($authorUuid)->willReturn($maybeUser);

        $maybeUser->getOrThrow(Argument::type(NotFoundException::class))->willReturn($author);

        $tweetRepository->store(Argument::type(Tweet::class))->shouldBeCalled();

        $clock->currentTime()->willReturn(new \DateTimeImmutable('now'))->shouldBeCalled();

        $eventDispatcher->dispatch(Argument::type(TweetPublished::class))->shouldBeCalled();

        $this->__invoke($command);
    }
}
