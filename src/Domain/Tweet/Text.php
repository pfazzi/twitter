<?php

declare(strict_types=1);

namespace App\Domain\Tweet;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class Text
{
    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $value;

    public function __construct(string $text)
    {
        Assert::maxLength($text, 100, 'Max Characters per tweet exceeded');

        $this->value = $text;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public static function fromString(string $text): self
    {
        return new self($text);
    }
}
