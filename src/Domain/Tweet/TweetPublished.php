<?php

declare(strict_types=1);

namespace App\Domain\Tweet;

use Ramsey\Uuid\UuidInterface;

class TweetPublished
{
    private UuidInterface $tweetUuid;

    public function __construct(UuidInterface $tweetUuid)
    {
        $this->tweetUuid = $tweetUuid;
    }

    public function getTweetUuid(): UuidInterface
    {
        return $this->tweetUuid;
    }
}
