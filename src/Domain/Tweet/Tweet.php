<?php

declare(strict_types=1);

namespace App\Domain\Tweet;

use App\Domain\User\User;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @Serializer\ExclusionPolicy("all")
 */
class Tweet
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private UuidInterface $uuid;

    /**
     * @ORM\Embedded(class="App\Domain\Tweet\Text", columnPrefix=false)
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private Text $text;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     * @Serializer\Expose()
     */
    private \DateTimeImmutable $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\User\User")
     * @ORM\JoinColumn(name="author_uuid", referencedColumnName="uuid")
     * @Serializer\Expose()
     */
    private User $author;

    private function __construct(UuidInterface $uuid, Text $text, \DateTimeImmutable $createdAt, User $author)
    {
        $this->uuid = $uuid;
        $this->text = $text;
        $this->createdAt = $createdAt;
        $this->author = $author;
    }

    public static function publish(UuidInterface $uuid, Text $text, \DateTimeImmutable $createdAt, User $author)
    {
        return new self($uuid, $text, $createdAt, $author);
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getText(): Text
    {
        return $this->text;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function changeText(Text $newText): void
    {
        $this->text = $newText;
    }
}
