<?php

declare(strict_types=1);

namespace App\Domain\Tweet;

use PhpOption\Option;
use Ramsey\Uuid\UuidInterface;

interface TweetRepositoryInterface
{
    public function store(Tweet $tweet): void;

    /**
     * @return Option<Tweet>
     */
    public function get(UuidInterface $uuid);

    public function pagedResultForUser(UuidInterface $recipientUuid, int $page, int $limit);
}
