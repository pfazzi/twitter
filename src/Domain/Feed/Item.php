<?php

declare(strict_types=1);

namespace App\Domain\Feed;

use App\Domain\Tweet\Tweet;
use App\Domain\User\User;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @Serializer\ExclusionPolicy("all")
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @Serializer\Type("string")
     */
    private UuidInterface $uuid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\User\User")
     * @ORM\JoinColumn(referencedColumnName="uuid")
     */
    private User $recipient;

    /**
     * @ORM\ManyToOne(targetEntity="App\Domain\Tweet\Tweet")
     * @ORM\JoinColumn(referencedColumnName="uuid")
     * @Serializer\Expose()
     */
    private Tweet $tweet;

    /**
     * @ORM\Embedded(class="Rating", columnPrefix=false)
     */
    private Rating $rating;

    private function __construct(UuidInterface $uuid, User $recipient, Tweet $tweet, Rating $rating)
    {
        $this->uuid = $uuid;
        $this->recipient = $recipient;
        $this->tweet = $tweet;
        $this->rating = $rating;
    }

    public static function create(UuidInterface $uuid, User $recipient, Tweet $tweet, Rating $rating): self
    {
        return new self($uuid, $recipient, $tweet, $rating);
    }

    public function getRecipient(): User
    {
        return $this->recipient;
    }

    public function getTweet(): Tweet
    {
        return $this->tweet;
    }

    public function getRating(): Rating
    {
        return $this->rating;
    }
}
