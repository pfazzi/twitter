<?php

declare(strict_types=1);

namespace App\Domain\Feed;

use App\Domain\Tweet\Tweet;
use App\Domain\User\User;

interface RatingCalculatorInterface
{
    public function calculateRateFor(Tweet $tweet, User $user): Rating;
}
