<?php

declare(strict_types=1);

namespace App\Domain\Feed;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class Rating
{
    /**
     * @ORM\Column(name="rating", type="decimal")
     */
    private float $value;

    public function __construct(float $rating)
    {
        Assert::range($rating, 1, 5, 'Rating must be a value in range [1, 5]');

        $this->value = $rating;
    }

    public function toFloat(): float
    {
        return $this->value;
    }

    public static function fromFloat(float $rating): self
    {
        return new self($rating);
    }
}
