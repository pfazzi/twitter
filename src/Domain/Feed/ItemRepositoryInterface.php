<?php

declare(strict_types=1);

namespace App\Domain\Feed;

interface ItemRepositoryInterface
{
    public function store(Item $item): void;
}
