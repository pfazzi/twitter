<?php

declare(strict_types=1);

namespace App\Domain\Shared;

interface EventDispatcherInterface
{
    public function dispatch($event): void;
}
