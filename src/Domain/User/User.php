<?php

declare(strict_types=1);

namespace App\Domain\User;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity
 * @Serializer\ExclusionPolicy("all")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid")
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private UuidInterface $uuid;

    /**
     * @ORM\Embedded(class="App\Domain\User\Username", columnPrefix=false)
     * @Serializer\Type("string")
     * @Serializer\Expose()
     */
    private Username $username;

    /**
     * @ORM\Embedded(class="App\Domain\User\Email", columnPrefix=false)
     */
    private Email $email;

    public function __construct(UuidInterface $uuid, Username $username, Email $email)
    {
        $this->uuid = $uuid;
        $this->username = $username;
        $this->email = $email;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function getUsername(): Username
    {
        return $this->username;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }
}
