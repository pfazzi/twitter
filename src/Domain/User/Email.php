<?php

declare(strict_types=1);

namespace App\Domain\User;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class Email
{
    /**
     * @ORM\Column(name="email", nullable=false)
     */
    private string $value;

    public function __construct(string $email)
    {
        Assert::email($email);

        $this->value = $email;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public static function fromString(string $email): self
    {
        return new self($email);
    }
}
