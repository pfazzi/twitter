<?php

declare(strict_types=1);

namespace App\Domain\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class Username
{
    /**
     * @ORM\Column(name="username", nullable=false)
     */
    private string $value;

    public function __construct(string $username)
    {
        // TODO: check length and add proper mapping metadata

        $this->value = $username;
    }

    public function __toString()
    {
        return $this->value;
    }

    public function toString(): string
    {
        return $this->value;
    }

    public static function fromString(string $username): self
    {
        return new self($username);
    }
}
