<?php

declare(strict_types=1);

namespace App\Domain\User;

use PhpOption\Option;
use Ramsey\Uuid\UuidInterface;

interface UserRepositoryInterface
{
    /**
     * @return Option<User>
     */
    public function get(UuidInterface $uuid): Option;

    public function store(User $user): void;

    /**
     * @return iterable<int, User>
     */
    public function all(): iterable;
}
