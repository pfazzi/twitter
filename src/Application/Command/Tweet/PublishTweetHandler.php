<?php

declare(strict_types=1);

namespace App\Application\Command\Tweet;

use App\Domain\Shared\ClockInterface;
use App\Domain\Shared\EventDispatcherInterface;
use App\Domain\Shared\NotFoundException;
use App\Domain\Tweet\Tweet;
use App\Domain\Tweet\TweetPublished;
use App\Domain\Tweet\TweetRepositoryInterface;
use App\Domain\User\UserRepositoryInterface;
use Ramsey\Uuid\Uuid;

class PublishTweetHandler
{
    private UserRepositoryInterface $userRepository;
    private TweetRepositoryInterface $tweetRepository;
    private ClockInterface $clock;
    /**
     * @var EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        UserRepositoryInterface $userRepository,
        TweetRepositoryInterface $tweetRepository,
        ClockInterface $clock,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->tweetRepository = $tweetRepository;
        $this->clock = $clock;
        $this->userRepository = $userRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function __invoke(PublishTweet $command): void
    {
        $maybeAuthor = $this->userRepository->get($command->getAuthorUuid());

        $newTweet = Tweet::publish(
            $uuid = Uuid::uuid4(),
            $command->getText(),
            $this->clock->currentTime(),
            $maybeAuthor->getOrThrow(new NotFoundException())
        );

        $this->eventDispatcher->dispatch(
            new TweetPublished($uuid)
        );

        $this->tweetRepository->store($newTweet);
    }
}
