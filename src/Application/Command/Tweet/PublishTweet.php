<?php

declare(strict_types=1);

namespace App\Application\Command\Tweet;

use App\Domain\Tweet\Text;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class PublishTweet
{
    private Text $text;
    private UuidInterface $authorUuid;

    public function __construct(string $text, string $authorUuid)
    {
        $this->text = Text::fromString($text);
        $this->authorUuid = Uuid::fromString($authorUuid);
    }

    public function getText(): Text
    {
        return $this->text;
    }

    public function getAuthorUuid(): UuidInterface
    {
        return $this->authorUuid;
    }
}
