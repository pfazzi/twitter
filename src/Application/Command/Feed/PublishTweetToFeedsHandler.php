<?php

declare(strict_types=1);

namespace App\Application\Command\Feed;

use App\Domain\Feed\Item;
use App\Domain\Feed\ItemRepositoryInterface;
use App\Domain\Feed\RatingCalculatorInterface;
use App\Domain\Shared\NotFoundException;
use App\Domain\Tweet\TweetRepositoryInterface;
use App\Domain\User\UserRepositoryInterface;
use Ramsey\Uuid\Uuid;

class PublishTweetToFeedsHandler
{
    private RatingCalculatorInterface $ratingCalculator;
    private UserRepositoryInterface $userRepository;
    private TweetRepositoryInterface $tweetRepository;
    private ItemRepositoryInterface $itemRepository;

    public function __construct(
        RatingCalculatorInterface $ratingCalculator,
        UserRepositoryInterface $userRepository,
        TweetRepositoryInterface $tweetRepository,
        ItemRepositoryInterface $itemRepository
    ) {
        $this->ratingCalculator = $ratingCalculator;
        $this->userRepository = $userRepository;
        $this->tweetRepository = $tweetRepository;
        $this->itemRepository = $itemRepository;
    }

    public function __invoke(PublishTweetToFeeds $command): void
    {
        $tweet = $this->tweetRepository
            ->get($command->getTweetUuid())
            ->getOrThrow(new NotFoundException());

        foreach ($this->userRepository->all() as $recipientUser) {
            $rating = $this->ratingCalculator->calculateRateFor($tweet, $recipientUser);

            $item = Item::create(
                Uuid::uuid4(),
                $recipientUser,
                $tweet,
                $rating
            );

            $this->itemRepository->store($item);
        }
    }
}
