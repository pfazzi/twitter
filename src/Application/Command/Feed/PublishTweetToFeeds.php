<?php

declare(strict_types=1);

namespace App\Application\Command\Feed;

use Ramsey\Uuid\UuidInterface;

class PublishTweetToFeeds
{
    private UuidInterface $tweetUuid;

    public function __construct(UuidInterface $tweetUuid)
    {
        $this->tweetUuid = $tweetUuid;
    }

    public function getTweetUuid(): UuidInterface
    {
        return $this->tweetUuid;
    }
}
