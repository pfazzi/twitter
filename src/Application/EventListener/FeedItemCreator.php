<?php

declare(strict_types=1);

namespace App\Application\EventListener;

use App\Application\Command\Feed\PublishTweetToFeeds;
use App\Domain\Tweet\TweetPublished;
use Symfony\Component\Messenger\MessageBusInterface;

class FeedItemCreator
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function __invoke(TweetPublished $event): void
    {
        $this->messageBus->dispatch(
            new PublishTweetToFeeds($event->getTweetUuid())
        );
    }
}
