<?php

declare(strict_types=1);

namespace App\Interfaces\Http\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AppController extends AbstractController
{
    public function __invoke(): Response
    {
        return $this->render('base.html.twig', []);
    }
}
