<?php

declare(strict_types=1);

namespace App\Interfaces\Http\Rest\Controller;

use App\Application\Command\Tweet\PublishTweet;
use FOS\RestBundle\Controller\ControllerTrait;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Webmozart\Assert\Assert;

class NewTweetController
{
    use ControllerTrait;

    private MessageBusInterface $messageBus;

    public function __construct(ViewHandlerInterface $viewHandler, MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
        $this->setViewHandler($viewHandler);
    }

    public function __invoke(Request $request): Response
    {
        $text = $request->get('text');
        $authorUuid = $request->get('authorUuid');

        Assert::notNull($text, 'Text must not be null');
        Assert::notNull($authorUuid, 'Author UUID must be a valid UUID');

        $this->messageBus->dispatch(new PublishTweet($text, $authorUuid));

        return $this->handleView(
            View::create(null, Response::HTTP_CREATED)
        );
    }
}
