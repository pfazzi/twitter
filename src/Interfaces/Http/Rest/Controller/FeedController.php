<?php

declare(strict_types=1);

namespace App\Interfaces\Http\Rest\Controller;

use App\Domain\Tweet\TweetRepositoryInterface;
use FOS\RestBundle\Controller\ControllerTrait;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

class FeedController
{
    use ControllerTrait;

    private TweetRepositoryInterface $tweetRepository;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        TweetRepositoryInterface $tweetRepository
    ) {
        $this->setViewHandler($viewHandler);
        $this->tweetRepository = $tweetRepository;
    }

    public function __invoke(Request $request): Response
    {
        $userUuid = $request->get('userUuid'); // TODO: replace with userUuid
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 10);

        Assert::notEmpty($userUuid, 'User uuid not provided');

        $page = $this->tweetRepository->pagedResultForUser(
            Uuid::fromString($userUuid),
            $page,
            $limit
        );

        return $this->handleView(
            View::create($page, Response::HTTP_OK)
        );
    }
}
