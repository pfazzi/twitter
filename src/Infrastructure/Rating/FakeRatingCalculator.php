<?php

declare(strict_types=1);

namespace App\Infrastructure\Rating;

use App\Domain\Feed\Rating;
use App\Domain\Feed\RatingCalculatorInterface;
use App\Domain\Tweet\Tweet;
use App\Domain\User\User;

class FakeRatingCalculator implements RatingCalculatorInterface
{
    public function calculateRateFor(Tweet $tweet, User $user): Rating
    {
        return Rating::fromFloat(random_int(1, 5));
    }
}
