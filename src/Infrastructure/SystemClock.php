<?php

declare(strict_types=1);

namespace App\Infrastructure;

use App\Domain\Shared\ClockInterface;

class SystemClock implements ClockInterface
{
    public function currentTime(): \DateTimeImmutable
    {
        return new \DateTimeImmutable('now');
    }
}
