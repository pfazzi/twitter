<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200219210907 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create schema';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE item (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', recipient_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', tweet_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', rating NUMERIC(10, 0) NOT NULL, INDEX IDX_1F1B251EE92F8F78 (recipient_id), INDEX IDX_1F1B251E1041E39B (tweet_id), PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', username VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tweet (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', author_uuid CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', value VARCHAR(100) NOT NULL, INDEX IDX_3D660A3B3590D879 (author_uuid), PRIMARY KEY(uuid)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251EE92F8F78 FOREIGN KEY (recipient_id) REFERENCES user (uuid)');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251E1041E39B FOREIGN KEY (tweet_id) REFERENCES tweet (uuid)');
        $this->addSql('ALTER TABLE tweet ADD CONSTRAINT FK_3D660A3B3590D879 FOREIGN KEY (author_uuid) REFERENCES user (uuid)');

        $this->addSql("INSERT INTO user VALUES('d73d0b19-17f5-4221-8096-b28346837b54', 'pfazzi', 'pfazzi@example.com');");
        $this->addSql("INSERT INTO user VALUES('f25cf913-50e6-44ec-9692-487dee0df17c', 'patrick', 'patrick@example.com');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251EE92F8F78');
        $this->addSql('ALTER TABLE tweet DROP FOREIGN KEY FK_3D660A3B3590D879');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251E1041E39B');
        $this->addSql('DROP TABLE item');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE tweet');
    }
}
