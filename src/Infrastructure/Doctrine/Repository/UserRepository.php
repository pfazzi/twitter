<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Doctrine\Persistence\ObjectRepository;
use PhpOption\Option;
use Ramsey\Uuid\UuidInterface;

class UserRepository implements UserRepositoryInterface
{
    private ObjectRepository $doctrineRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->doctrineRepository = $entityManager->getRepository(User::class);
    }

    /**
     * @psalm-suppress MoreSpecificReturnType
     * @psalm-suppress LessSpecificReturnStatement
     */
    public function get(UuidInterface $uuid): Option
    {
        return Option::fromValue($this->doctrineRepository->find($uuid));
    }

    public function store(User $user): void
    {
        $this->entityManager->persist($user);
    }

    public function all(): iterable
    {
        $dql = 'SELECT user FROM App\Domain\User\User as user';

        return self::flattenDoctrineIterator(
            $this->entityManager->createQuery($dql)->iterate()
        );
    }

    private static function flattenDoctrineIterator(IterableResult $iterableResult): \Iterator
    {
        return new class($iterableResult) implements \Iterator {
            private IterableResult $doctrineIterator;

            public function __construct(IterableResult $doctrineIterator)
            {
                $this->doctrineIterator = $doctrineIterator;
            }

            public function current()
            {
                return $this->doctrineIterator->current()[0];
            }

            public function next()
            {
                $next = $this->doctrineIterator->next();

                return false === $next ? null : $next[0];
            }

            public function key()
            {
                return $this->doctrineIterator->key();
            }

            public function valid()
            {
                return $this->doctrineIterator->valid();
            }

            public function rewind(): void
            {
                $this->doctrineIterator->rewind();
            }
        };
    }
}
