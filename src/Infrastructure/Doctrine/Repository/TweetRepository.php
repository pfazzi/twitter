<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Tweet\Tweet;
use App\Domain\Tweet\TweetRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PhpOption\Option;
use Ramsey\Uuid\UuidInterface;

class TweetRepository implements TweetRepositoryInterface
{
    private ObjectRepository $doctrineRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->doctrineRepository = $entityManager->getRepository(Tweet::class);
    }

    public function store(Tweet $tweet): void
    {
        $this->entityManager->persist($tweet);
    }

    /**
     * @psalm-suppress MoreSpecificReturnType
     * @psalm-suppress LessSpecificReturnStatement
     */
    public function get(UuidInterface $uuid)
    {
        return Option::fromValue($this->doctrineRepository->find($uuid));
    }

    public function pagedResultForUser(UuidInterface $recipientUuid, int $page, int $limit)
    {
        $dql = <<<DQL
                SELECT tweet 
                FROM App\Domain\Feed\Item item
                    JOIN App\Domain\User\User recipient WITH item.recipient = recipient
                    JOIN App\Domain\Tweet\Tweet tweet WITH item.tweet = tweet
                WHERE recipient.uuid = :recipientUuid
                ORDER BY item.rating.value DESC
            DQL;

        return $this->entityManager
            ->createQuery($dql)
            ->setParameter('recipientUuid', $recipientUuid)
            ->setFirstResult(($page - 1) * $limit)
            ->setMaxResults($limit)
            ->getResult();
    }
}
