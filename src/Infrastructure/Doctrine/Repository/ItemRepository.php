<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Domain\Feed\Item;
use App\Domain\Feed\ItemRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

class ItemRepository implements ItemRepositoryInterface
{
    private ObjectRepository $doctrineRepository;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->doctrineRepository = $entityManager->getRepository(Item::class);
    }

    public function store(Item $item): void
    {
        $this->entityManager->persist($item);
    }
}
