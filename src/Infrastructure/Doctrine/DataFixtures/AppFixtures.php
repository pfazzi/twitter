<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\DataFixtures;

use App\Domain\User\Email;
use App\Domain\User\User;
use App\Domain\User\Username;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $pfazzi = new User(
            Uuid::fromString('d73d0b19-17f5-4221-8096-b28346837b54'),
            Username::fromString('pfazzi'),
            Email::fromString('pfazzi@example.com')
        );

        $manager->persist($pfazzi);

        $patrick = new User(
            Uuid::fromString('f25cf913-50e6-44ec-9692-487dee0df17c'),
            Username::fromString('patrick'),
            Email::fromString('patrick@example.com')
        );

        $manager->persist($patrick);

        $manager->flush();
    }
}
