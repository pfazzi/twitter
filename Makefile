run: run_docker run_symfony_proxy run_symfony_ws

stop: stop_symfony_ws stop_docker

run_symfony_proxy:
	symfony proxy:start

run_symfony_ws:
	symfony server:start -d

run_docker:
	docker-compose up -d

stop_docker:
	docker-compose stop

stop_symfony_ws:
	symfony server:stop

# QUALITY TOOLS

phpspec:
	symfony php vendor/bin/phpspec run

phpunit:
	symfony php bin/phpunit

test: phpspec phpunit

psalm:
	symfony php vendor/bin/psalm --show-info=false --threads=4

php_cs_fixer_dry_run:
	symfony php vendor/bin/php-cs-fixer fix --diff --dry-run -v

php_cs_fixer:
	symfony php vendor/bin/php-cs-fixer fix

qa: php_cs_fixer_dry_run psalm phpspec phpunit

# Utilities

refresh:
	symfony console cache:clear

# Lint

lint: lint_yaml lint_container

lint_yaml:
	symfony console lint:yaml config --parse-tags

lint_container:
	symfony console lint:container

# Doctrine

doctrine_database_create:
	symfony console doctrine:database:create --no-interaction

doctrine_database_drop:
	symfony console doctrine:database:drop --force

doctrine_migrations_diff:
	symfony console doctrine:migrations:diff

doctrine_migrations_migrate:
	symfony console doctrine:migrations:migrate --no-interaction

doctrine_schema_update:
	symfony console doctrine:schema:update --dump-sql
	symfony console doctrine:schema:update --force

doctrine_schema_validate:
	symfony console doctrine:schema:validate --skip-sync -vvv --no-interaction

doctrine_fixtures_load:
	symfony php bin/console doctrine:fixtures:load
