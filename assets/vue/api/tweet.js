import axios from "axios";
export default {
  findAll(userUuid) {
    return axios.get(`/api/feed?userUuid=${userUuid}`);
  },
  publish(authorUuid, text) {
    return axios.post("/api/tweets", {
      authorUuid,
      text
    });
  },
};
