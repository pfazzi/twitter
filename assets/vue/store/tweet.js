import TweetAPI from "../api/tweet";
import moment from "moment";

const PUBLISHING_TWEET = "PUBLISHING_TWEET";
const PUBLISHING_TWEET_SUCCESS = "PUBLISHING_TWEET_SUCCESS";
const PUBLISHING_TWEET_ERROR = "PUBLISHING_TWEET_ERROR";
const FETCHING_TWEETS = "FETCHING_TWEETS";
const FETCHING_TWEETS_SUCCESS = "FETCHING_TWEETS_SUCCESS";
const FETCHING_TWEETS_ERROR = "FETCHING_TWEETS_ERROR";

export default {
  namespaced: true,
  state: {
    isLoading: false,
    error: null,
    tweets: []
  },
  getters: {
    isLoading(state) {
      return state.isLoading;
    },
    hasError(state) {
      return state.error !== null;
    },
    error(state) {
      return state.error;
    },
    hasTweets(state) {
      return state.tweets.length > 0;
    },
    tweets(state) {
      return state.tweets;
    },
  },
  mutations: {
    [PUBLISHING_TWEET](state) {
      state.isLoading = true;
      state.error = null;
    },
    [PUBLISHING_TWEET_SUCCESS](state, {author, text}) {
      state.isLoading = false;
      state.error = null;
      state.tweets.unshift({
        text,
        created_at: moment().format(),
        author: author
      });
    },
    [PUBLISHING_TWEET_ERROR](state, error) {
      state.isLoading = false;
      state.error = error;
      state.tweets = [];
    },
    [FETCHING_TWEETS](state) {
      state.isLoading = true;
      state.error = null;
      state.tweets = [];
    },
    [FETCHING_TWEETS_SUCCESS](state, tweets) {
      state.isLoading = false;
      state.error = null;
      state.tweets = tweets;
    },
    [FETCHING_TWEETS_ERROR](state, error) {
      state.isLoading = false;
      state.error = error;
      state.tweets = [];
    }
  },
  actions: {
    async publish({ commit }, {author, text}) {
      commit(PUBLISHING_TWEET);
      try {
        await TweetAPI.publish(author.uuid, text);
        commit(PUBLISHING_TWEET_SUCCESS, {author, text});
      } catch (error) {
        commit(PUBLISHING_TWEET_ERROR, error);
      }
    },
    async findAll({ commit }, userUuid) {
      commit(FETCHING_TWEETS);
      try {
        let response = await TweetAPI.findAll(userUuid);
        commit(FETCHING_TWEETS_SUCCESS, response.data);
        return response.data;
      } catch (error) {
        commit(FETCHING_TWEETS_ERROR, error);
      }
    }
  }
};
