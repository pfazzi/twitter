import Vue from "vue";
import Vuex from "vuex";
import UserModule from "./user";
import TweetModule from "./tweet";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user: UserModule,
    tweet: TweetModule
  }
});
