export default {
  namespaced: true,
  state: {
    currentUser: {
      username: 'patrick',
      uuid: 'f25cf913-50e6-44ec-9692-487dee0df17c'
    }
  },
  getters: {
    currentUser: state => state.currentUser
  }
}
