import Vue from "vue";
import App from "./App";
import store from "./store";
import moment from 'moment';


Vue.filter('fromNow', (value) => {
  if (value) {
    return moment(value).fromNow()
  }
});

new Vue({
  components: { App },
  template: "<App/>",
  store
}).$mount("#app");
