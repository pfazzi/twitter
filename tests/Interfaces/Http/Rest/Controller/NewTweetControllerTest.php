<?php

declare(strict_types=1);

namespace App\Tests\Interfaces\Http\Rest\Controller;

use App\Domain\User\Email;
use App\Domain\User\User;
use App\Domain\User\Username;
use App\Domain\User\UserRepositoryInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class NewTweetControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $user = new User(
            Uuid::fromString('351b9778-9f2f-4d0d-a8cc-6487e7418027'),
            Username::fromString('pfazzi'),
            Email::fromString('pfazzi@example.com')
        );

        self::$container->get(UserRepositoryInterface::class)->store($user);
    }

    public function testItShouldReturnSuccessfulResponse(): void
    {
        $this->client->request(
            'POST',
            '/api/tweets',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                    'text' => 'Lorem ipsum',
                    'authorUuid' => '351b9778-9f2f-4d0d-a8cc-6487e7418027',
                ])
        );

        self::assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }
}
