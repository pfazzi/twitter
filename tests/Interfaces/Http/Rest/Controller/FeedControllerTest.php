<?php

declare(strict_types=1);

namespace App\Tests\Interfaces\Http\Rest\Controller;

use App\Application\Command\Tweet\PublishTweet;
use App\Domain\User\Email;
use App\Domain\User\User;
use App\Domain\User\Username;
use App\Domain\User\UserRepositoryInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class FeedControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private UuidInterface $userId;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $pfazzi = new User(
            $this->userId = Uuid::uuid4(),
            Username::fromString('pfazzi'),
            Email::fromString('pfazzi@example.com')
        );

        $patrick = new User(
            Uuid::fromString($authorUuid = '351b9778-9f2f-4d0d-a8cc-6487e7418027'),
            Username::fromString('patrick'),
            Email::fromString('patrick@example.com')
        );
        self::$container->get(UserRepositoryInterface::class)->store($patrick);
        self::$container->get(UserRepositoryInterface::class)->store($pfazzi);

        $bus = self::$container->get(MessageBusInterface::class);

        $bus->dispatch(new PublishTweet('Lorem ipsum', $authorUuid));
        $bus->dispatch(new PublishTweet('Dolor sit et amet', $authorUuid));
    }

    public function testItReturnsAListOfTweets(): void
    {
        $this->client->request(
            'GET',
            '/api/feed?',
            [
                'userUuid' => $this->userId->toString(),
            ],
            [],
            ['ACCEPT' => 'application/json'],
        );

        self::assertResponseStatusCodeSame(Response::HTTP_OK);

        $items = json_decode($this->client->getResponse()->getContent(), true);

        self::assertCount(2, $items);
    }
}
